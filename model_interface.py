import time
import sys
import os

from models import Process, Argument, Keyword, engine

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import DataError
from sqlalchemy.exc import InternalError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import InvalidRequestError

import random
import string

db_session = sessionmaker()
db_session.configure(bind=engine)

session = db_session()


def add_process(codig_processo,
                numero_processo=None,
                uf=None,
                comarca=None,
                vara=None,
                juiz=None,
                juiz_type=None,
                data_sent=None,
                advogados=None,
                motivo=None,
                produto=None,
                pedido=None,
                resultado=None,
                valor_sent=None,
                argumentos=None,
                keywords=None):
    """Creates process entry."""

    joined_argumentos = None

    if argumentos:
        joined_argumentos = ''

        for argumento in argumentos:
            if not joined_argumentos:
                joined_argumentos = argumento['text']
            else:
                joined_argumentos += '; '+argumento['text']

    try:
        new_process = Process(codig_processo=codig_processo,
                              numero_processo=numero_processo,
                              uf=uf,
                              comarca=comarca,
                              vara=vara,
                              juiz=juiz,
                              juiz_type=juiz_type,
                              data_sent=data_sent,
                              advogados=advogados,
                              motivo=motivo,
                              produto=produto,
                              pedido=pedido,
                              resultado=resultado,
                              valor_sent=valor_sent,
                              argumentos=joined_argumentos)

        if argumentos:
            for argument in argumentos:
                new_process.arguments.append(
                    Argument(argument=argument['text'],
                             count=argument['count']))

        if keywords:
            for keyword in keywords:
                new_process.keywords.append(
                    Keyword(keyword=keyword['text'],
                            relevance=keyword['relevance']))

        session.add(new_process)
        session.commit()

        return new_process
    except IntegrityError as e:
        session.rollback()
        print(e)
        session.rollback()
        return None
    except InvalidRequestError as e:
        session.rollback()
        print(e)
        return None
    except:
        session.rollback()
        # print("Unexpected error:", sys.exc_info()[0])
        # print(codig_processo)
        # print(numero_processo)
        # print(uf),
        # print(comarca),
        # print(vara),
        # print(juiz),
        # print(juiz_type),
        # print(data_sent),
        # print(advogados),
        # print(motivo),
        # print(produto),
        # print(pedido),
        # print(resultado),
        # print(valor_sent),
        # print(joined_argumentos)
        # print('--------------')
        # print(argumentos)
        # print('--------------')
        # print(keywords)
        return None
