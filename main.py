import json
import os
from watson_developer_cloud import NaturalLanguageUnderstandingV1
import watson_developer_cloud.natural_language_understanding.features.v1 as \
    Features

from model_interface import add_process


if __name__ == '__main__':
    natural_language_understanding = NaturalLanguageUnderstandingV1(
        version='2017-02-27',
        username='915b391f-4993-4715-84ca-576f717d243e',
        password='Y61Y4QhBX5o0')

    data_field = None
    produto_field = None
    uf_field = None
    vara_field = None
    cidade_field = None
    numero_proc_field = None
    arg_field = None
    valor_field = None
    pedido_field = None
    resultado_field = None
    motivo_field = None
    reu_field = None
    autor_field = None
    comarca_field = None
    juiz_pessoa_field = None
    juiz_tipo_field = None
    keywords_field = None

    for sample in os.listdir("./sample"):
        if sample.endswith(".txt"):
            print(sample)
            
            with open('sample/'+sample, 'r', encoding='iso-8859-1') as process_file:
                text = process_file.read()

                response = natural_language_understanding.analyze(
                    text=text,
                    features=[
                        Features.Entities(
                            model='10:4bf90cf9-fd52-41d4-af16-8ea9fa031596'),
                        Features.Keywords()])

                # print(response)

                if 'entities' in response:
                    for entity in response['entities']:
                        if entity['type'] == 'DATA':
                            if not data_field:
                                data_field = entity['text']
                            elif entity['text'] not in data_field:
                                data_field += '; '+entity['text']

                        if entity['type'] == 'PRODUTO':
                            if not produto_field:
                                produto_field = entity['text']
                            elif entity['text'] not in produto_field:
                                produto_field += '; '+entity['text']

                        if entity['type'] == 'UF':
                            if not uf_field:
                                uf_field = entity['text']
                            elif entity['text'] not in uf_field:
                                uf_field += '; '+entity['text']

                        if entity['type'] == 'VARA':
                            if not vara_field:
                                vara_field = entity['text']
                            elif entity['text'] not in vara_field:
                                vara_field += '; '+entity['text']

                        if entity['type'] == 'CIDADE':
                            if not cidade_field:
                                cidade_field = entity['text']
                            elif entity['text'] not in cidade_field:
                                cidade_field += '; '+entity['text']

                        if entity['type'] == 'INDENTIFICAR':  #so much portuguese!
                            if not numero_proc_field:
                                numero_proc_field = entity['text']
                            elif entity['text'] not in numero_proc_field:
                                numero_proc_field += '; '+entity['text']

                        if entity['type'] == 'ARGUMENTOS':
                            if not arg_field:
                                arg_field = [entity]
                            else:
                                arg_field.append(entity)

                        if entity['type'] == 'VALOR':
                            if not valor_field:
                                valor_field = entity['text']
                            elif entity['text'] not in valor_field:
                                valor_field += '; '+entity['text']

                        if entity['type'] == 'PEDIDO':
                            if not pedido_field:
                                pedido_field = entity['text']
                            elif entity['text'] not in pedido_field:
                                pedido_field += '; '+entity['text']

                        if entity['type'] == 'RESULTADO':
                            if not resultado_field:
                                resultado_field = entity['text']
                            elif entity['text'] not in resultado_field:
                                resultado_field += '; '+entity['text']

                        if entity['type'] == 'MOTIVO':
                            if not motivo_field:
                                 motivo_field = entity['text']
                            elif entity['text'] not in motivo_field:
                                motivo_field += '; '+entity['text']

                        if entity['type'] == 'REU':
                            if not reu_field:
                                reu_field = entity['text']
                            elif entity['text'] not in reu_field:
                                reu_field += '; '+entity['text']

                        if entity['type'] == 'AUTOR':
                            if not autor_field:
                                autor_field = entity['text']
                            elif entity['text'] not in autor_field:
                                autor_field += '; '+entity['text']

                        if entity['type'] == 'COMARCA':
                            if not comarca_field:
                                comarca_field = entity['text']
                            elif entity['text'] not in comarca_field:
                                comarca_field += '; '+entity['text']

                        if (entity['type'] == 'JUIZ') and \
                                (entity['disambiguation']['subtype'] == 'PESSOA'):
                            if not juiz_pessoa_field:
                                juiz_pessoa_field = entity['text']
                            elif entity['text'] not in juiz_pessoa_field:
                                juiz_pessoa_field += '; '+entity['text']

                        if (entity['type'] == 'JUIZ') and \
                                (entity['disambiguation']['subtype'] == 'TIPO'):
                            if not juiz_tipo_field:
                                juiz_tipo_field = entity['text']
                            elif entity['text'] not in juiz_tipo_field:
                                juiz_tipo_field += '; '+entity['text']

                if 'keywords' in response:
                    for keyword in response['keywords']:
                        if not keywords_field:
                            keywords_field = [keyword]
                        else:
                            keywords_field.append(keyword)

                # print(data_field)
                # print(produto_field)
                # print(uf_field)
                # print(vara_field)
                # print(cidade_field)
                # print(numero_proc_field)
                # print(arg_field)
                # print(valor_field)
                # print(pedido_field)
                # print(resultado_field)
                # print(motivo_field)
                # print(reu_field)
                # print(autor_field)
                # print(comarca_field)
                # print(juiz_pessoa_field)
                # print(juiz_tipo_field)
                # print(keywords_field)

                # print(json.dumps(response, indent=2))

                add_process(codig_processo=sample.split('.')[0],
                            numero_processo=numero_proc_field,
                            uf=uf_field,
                            comarca=comarca_field,
                            vara=vara_field,
                            juiz=juiz_pessoa_field,
                            juiz_type=juiz_tipo_field,
                            data_sent=data_field,
                            advogados=None,
                            motivo=motivo_field,
                            produto=produto_field,
                            pedido=pedido_field,
                            resultado=resultado_field,
                            valor_sent=valor_field,
                            argumentos=arg_field,
                            keywords=keywords_field)
