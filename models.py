import sys
import logging
from sqlalchemy import (Column,
                       DateTime,
                       ForeignKey,
                       Integer,
                       String,
                       Numeric,
                       func)
from sqlalchemy import CheckConstraint

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import relationship, backref

from sqlalchemy import create_engine

Base = declarative_base()

# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

class Process(Base):
    """Process files

    Columns:
        id
        codig_processo
        numero_processo
        uf
        comarca
        vara
        juiz
        juiz_type
        data_sent
        advogados
        motivo
        produto
        pedido
        resultado
        valor_sent
        argumentos
    """

    __tablename__ = 'processes'

    id = Column(Integer, primary_key=True)

    codig_processo = Column(
        String(256),
        nullable=False,
        unique=True)

    numero_processo = Column(
        String(256),
        nullable=True)

    uf = Column(
        String(1024),
        nullable=True)

    comarca = Column(
        String(1024),
        nullable=True)

    vara = Column(
        String(1024),
        nullable=True)

    juiz = Column(
        String(1024),
        nullable=True)

    juiz_type = Column(
        String(1024),
        nullable=True)

    data_sent = Column(
        String(1024),
        nullable=True)

    advogados = Column(
        String(128),
        nullable=True)

    motivo = Column(
        String(1024),
        nullable=True)

    produto = Column(
        String(1024),
        nullable=True)

    pedido = Column(
        String(2048),
        nullable=True)

    resultado = Column(
        String(2048),
        nullable=True)

    valor_sent = Column(
        String(1024),
        nullable=True)

    argumentos = Column(
        String(4096),
        nullable=True)

    arguments = relationship(
        'Argument',
        back_populates='process',
        cascade='all, delete-orphan')

    keywords = relationship(
        'Keyword',
        back_populates='process',
        cascade='all, delete-orphan')

    def get_attributes_as_dictionary(self):
        return {
            'id': self.id,
            'codig_processo': self.codig_processo,
            'numero_processo': self.numero_processo,
            'uf': self.uf,
            'comarca': self.comarca,
            'vara': self.vara,
            'juiz': self.juiz,
            'juiz_type': self.juiz_type,
            'data_sent': self.data_sent,
            'advogados': self.advogados,
            'motivo': self.motivo,
            'produto': self.produto,
            'pedido': self.pedido,
            'resultado': self.resultado,
            'valor_sent': self.valor_sent,
            'argumentos': self.argumentos
        }

    def __repr__(self):
        return ('<Process(id={id}, '
                'codig_processo={codig_processo}, '
                'numero_processo={numero_processo}, '
                'uf={uf}, '
                'comarca={comarca}, '
                'vara={vara}, '
                'juiz={juiz}, '
                'juiz_type={juiz_type}, '
                'advogados={advogados}, '
                'motivo={motivo}, '
                'produto={produto}, '
                'pedido={pedido}, '
                'resultado={resultado}, '
                'valor_sent={valor_sent}, '
                'argumentos={argumentos}, '
                .format(**self.get_attributes_as_dictionary()))


class Keyword(Base):
    """Process files

    Columns:
        id
        process_id
        keyword,
        relevance
    """

    __tablename__ = 'keywords'

    id = Column(Integer, primary_key=True)

    process_id = Column(
        Integer,
        ForeignKey('processes.id', ondelete='CASCADE'))

    keyword = Column(
        String(512),
        nullable=False)

    relevance = Column(
        Numeric(4,2),
        nullable=False)

    process = relationship(
        'Process',
        back_populates='keywords')

    def get_attributes_as_dictionary(self):
        return {
            'process_id': self.process_id,
            'keyword': self.keyword
        }

    def __repr__(self):
        return ('<Keyword(process_id={process_id}, '
                'keyword={keyword}, '
                .format(**self.get_attributes_as_dictionary()))


class Argument(Base):
    """Process files

    Columns:
        id
        process_id
        argument
        count
    """

    __tablename__ = 'arguments'

    id = Column(Integer, primary_key=True)
    process_id = Column(
    Integer,
    ForeignKey('processes.id', ondelete='CASCADE'))

    argument = Column(
        String(512),
        nullable=True)

    count = Column(
        Integer,
        nullable=False)

    process = relationship(
        'Process',
        back_populates='arguments')

    def get_attributes_as_dictionary(self):
        return {
            'process_id': self.process_id,
            'argument': self.argument
        }

    def __repr__(self):
        return ('<Keyword(process_id={process_id}, '
                'argument={argument}, '
                .format(**self.get_attributes_as_dictionary()))

engine = create_engine('mysql+mysqldb://developer:oFFm!f51d2@Mc52Bv3@localhost/serasa_prototype')
Base.metadata.create_all(engine)
